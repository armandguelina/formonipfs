import * as IPFS from 'ipfs-core'
import express from 'express'
import bodyParser from 'body-parser'
const app = express()
const ipfs = await IPFS.create()
const gateway = 'https://ipfs.io/ipfs/'

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.send(`
    <form action="/" method="POST">
      <input type="text" name="content" />
      <input type="submit" value="Submit" />
    </form>
  `)
})

app.post('/', async (req, res) => {
  const content = req.body.content;
  
  console.log(`Name: ${content}`);
  const { cid }  = await ipfs.add(content)
  const ipns = await ipfs.name.publish(cid)
  res.send(`Content saved to IPFS and published to IPNS: `+cid)
  console.log(gateway+cid)
})

app.listen(3000, () => {
  console.log('Server listening on port 3000')
})

